﻿
USE [master]
GO

/****** Object:  Database [MagasinDB]    Script Date: 10/03/2018 16:02:11 ******/
CREATE DATABASE [MagasinDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MagasinDB', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.MSSQLSERVER2\MSSQL\DATA\MagasinDB.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MagasinDB_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL11.MSSQLSERVER2\MSSQL\DATA\MagasinDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [MagasinDB] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MagasinDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [MagasinDB] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [MagasinDB] SET ANSI_NULLS OFF
GO

ALTER DATABASE [MagasinDB] SET ANSI_PADDING OFF
GO

ALTER DATABASE [MagasinDB] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [MagasinDB] SET ARITHABORT OFF
GO

ALTER DATABASE [MagasinDB] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [MagasinDB] SET AUTO_CREATE_STATISTICS ON
GO

ALTER DATABASE [MagasinDB] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [MagasinDB] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [MagasinDB] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [MagasinDB] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [MagasinDB] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [MagasinDB] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [MagasinDB] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [MagasinDB] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [MagasinDB] SET  DISABLE_BROKER
GO

ALTER DATABASE [MagasinDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [MagasinDB] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [MagasinDB] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [MagasinDB] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [MagasinDB] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [MagasinDB] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [MagasinDB] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [MagasinDB] SET RECOVERY SIMPLE
GO

ALTER DATABASE [MagasinDB] SET  MULTI_USER
GO

ALTER DATABASE [MagasinDB] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [MagasinDB] SET DB_CHAINING OFF
GO

ALTER DATABASE [MagasinDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF )
GO

ALTER DATABASE [MagasinDB] SET TARGET_RECOVERY_TIME = 0 SECONDS
GO

USE [MagasinDB]
GO

/****** Object:  Table [dbo].[Categorie]    Script Date: 10/03/2018 16:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Commande]    Script Date: 10/03/2018 16:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Magasin]    Script Date: 10/03/2018 16:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Objet]    Script Date: 10/03/2018 16:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Profil]    Script Date: 10/03/2018 16:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[User]    Script Date: 10/03/2018 16:02:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

USE [master]
GO

ALTER DATABASE [MagasinDB] SET  READ_WRITE
GO
