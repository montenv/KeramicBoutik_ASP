﻿CREATE TABLE [dbo].[Objet](
	[idObjet] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](50) NOT NULL,
	[prix] [float] NOT NULL,
	[photo] [varchar](50) NULL,
	
	[fk_Categorie] INT          NULL,
    [fk_Commande]  INT          NULL,
    [fk_Magasin]  INT          NULL,
    CONSTRAINT [PK_Objet] PRIMARY KEY CLUSTERED ([idObjet] ASC),
    CONSTRAINT [FK_Objet_Categorie] FOREIGN KEY ([fk_Categorie]) REFERENCES [dbo].[Categorie] ([idCategorie]),
    CONSTRAINT [FK_Objet_Commande] FOREIGN KEY ([fk_Commande]) REFERENCES [dbo].[Commande] ([idCommande]),
    CONSTRAINT [FK_Objet_Magasin] FOREIGN KEY ([fk_Magasin]) REFERENCES [dbo].[Magasin] ([idMagasin]))