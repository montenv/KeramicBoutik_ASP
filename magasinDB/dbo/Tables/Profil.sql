﻿CREATE TABLE [dbo].[Profil](
[idProfil] [int] IDENTITY(1,1) NOT NULL,
[nom] [varchar](50) NOT NULL,
[prenom] [varchar](50) NOT NULL,
[email] [varchar](50) NOT NULL,
[fk_User]          INT           NOT NULL,
    CONSTRAINT [PK_Profil] PRIMARY KEY CLUSTERED ([idProfil] ASC),
    CONSTRAINT [FK_Profil_User] FOREIGN KEY ([fk_User]) REFERENCES [dbo].[User] ([idUser])
);