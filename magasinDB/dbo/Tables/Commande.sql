﻿CREATE TABLE [dbo].[Commande](
	[idCommande] [int] IDENTITY(1,1) NOT NULL,
	[dateCommande] [date] NOT NULL,
 CONSTRAINT [PK_Commande] PRIMARY KEY CLUSTERED 
(
	[idCommande] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Commande]  WITH CHECK ADD  CONSTRAINT [FK_Commande_Profil] FOREIGN KEY([idCommande])
REFERENCES [dbo].[Profil] ([idProfil])
GO

ALTER TABLE [dbo].[Commande] CHECK CONSTRAINT [FK_Commande_Profil]