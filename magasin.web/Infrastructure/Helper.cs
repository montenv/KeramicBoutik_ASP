﻿using magasin.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace magasin.web.Infrastructure
{
    public static class Helper
    {

        public static MvcHtmlString EditSelectNomCategorie(this HtmlHelper html, List<Categorie> listCat, int categorieSelected)
        {
            //TagBuilder FrmGroup = new TagBuilder("form"); //tu ne le mets pas si tu veux insérer ton sélect dans un formulaire déjà existant
            //FrmGroup.MergeAttribute("action", "Index");
            //FrmGroup.MergeAttribute("method", "post");
            TagBuilder select = new TagBuilder("select");
            TagBuilder option = new TagBuilder("option");
            option.SetInnerText("choisissez votre categorie");
            option.MergeAttribute("value", "");
            select.InnerHtml += option.ToString();
            foreach (Categorie cat in listCat)
            {
                option = new TagBuilder("option");
                option.SetInnerText(cat.NomCategorie);
                option.MergeAttribute("value", cat.IdCategorie.ToString());
                //Ici c'est pour quand on veut savoir ce que l'utilisateur avait déjà sélectionné comme option (dans le cas d'un update par exemple)
                if (categorieSelected == cat.IdCategorie) option.Attributes.Add("selected", "true");
                select.InnerHtml += option.ToString();
            }
            select.Attributes.Add("name", "Obj.Categorie.IdCategorie");
            return new MvcHtmlString(select.ToString());
        }

    }
}