﻿using magasin.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace magasin.web.Infrastructure
{
    public static class SessionUtils
    {

        public static Profil UserConnected
        {
            get
            {

                HttpContext.Current.Session["userConnected"] = HttpContext.Current.Session["userConnected"] ?? new Profil();
                return (Profil)HttpContext.Current.Session["userConnected"];
            }
            set { HttpContext.Current.Session["userConnected"] = value; }

        }
        public static List<DAL.Object> Panier
        {
            get
            {
                HttpContext.Current.Session["userPanier"] = HttpContext.Current.Session["userPanier"] ?? new List<DAL.Object>();
                return (List<DAL.Object>)HttpContext.Current.Session["userPanier"];
            }

        }
    }
}