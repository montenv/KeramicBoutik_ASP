﻿using magasin.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace magasin.web.Models
{
    public class ObjectCategorie
    {
        public DAL.Object Obj {get;set;}
        public List<Categorie> Cats { get; set; }

        public ObjectCategorie()
        {
            Obj = new DAL.Object();
        }
        public ObjectCategorie(DAL.Object obj, List<Categorie> cats)
        {
            Obj = obj;
            Cats = cats;
        }
       
    }
}