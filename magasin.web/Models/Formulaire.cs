﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace magasin.web.Models
{
    public class Formulaire
    {
        [Required]
        [StringLength(25)]
        public string Login { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Trop long")]
        public string Password { get; set; }

        [Required]
        [StringLength(25)]
        public string Nom { get; set; }

        [Required]
        [StringLength(25, ErrorMessage = "Trop long")]
        public string Prenom { get; set; }

        
        [Required]
        [EmailAddress]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Must be a valid Email Address")]
        public string Email { get; set; }

        
    }
}