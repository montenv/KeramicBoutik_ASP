﻿using magasin.DAL;
using magasin.web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace magasin.web.Controllers
{
    public class UserController : Controller
    {
        private DAOUser daoUser = new DAOUser();


        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Connection()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Connection(string login, string password)
        {
            Session["admin"] = "";
            Profil profilTrouve = daoUser.Connection(login, password);
            if (profilTrouve != null)
            {
                SessionUtils.UserConnected = profilTrouve;
                // quand le pro est null ca veut dire que le login ne marche pas
                // alors il faut pas controler le profile mais arrêter l'application & mettre erreur sur le page
                Session["profil"] = profilTrouve.Prenom + " " + profilTrouve.Nom.Substring(0, 1);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("About", "Home");
            }


        }

        public ActionResult Deconnexion()
        {
            Session["userConnected"] = null;
            return RedirectToAction("Index", "Home");
        }

    }
}

