﻿using magasin.DAL;
using magasin.web.Infrastructure;
using magasin.web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace magasin.web.Controllers
{
    public class ObjectController : Controller
    {
        private DAOObjet daoObjet = new DAOObjet();
        private DAOCategorie daoCat = new DAOCategorie();

        [HttpGet]
        public ActionResult CreerObjetPHOTO()
        {

            ObjectCategorie oc = new ObjectCategorie();
            oc.Cats = daoCat.readAll();

            return View(oc);
        }

        [HttpPost]
        public ActionResult CreerObjetPHOTO(ObjectCategorie Objectcat, HttpPostedFileBase file)
        {

            var allowedExtensions = new[] {
                    ".Jpg", ".png", ".jpg", ".jpeg"
                        };

            Objectcat.Obj.Photo = file.ToString(); //acceder a l'url  complet

            var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
            var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
            if (allowedExtensions.Contains(ext)) //permet de verifier l'extension de notre fichier   
            {
                string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                                                                          /*string myfile = name + "_" + ext;*/ //appending the name with id  
                string myfile = name + ext;
                // store the file inside ~/project folder(Img)  
                var path = Path.Combine(Server.MapPath("~/images"), file.FileName);
                Objectcat.Obj.Photo = "images/" + myfile;



                file.SaveAs(path);

                //img = DAOObjet.UploadImage(img);
            }
            else
            {
                ViewBag.message = "Please choose only Image file";
            }

            DAL.Object objectEncode = daoObjet.create(Objectcat.Obj);
            //return View();
            return RedirectToAction("AfficherObjet");

        }






        [HttpGet]
        public ActionResult AfficherObjet()
        {

            List<DAL.Object> listObj = new List<DAL.Object>();
            listObj = daoObjet.readAll();

            return View(listObj);
        }

        [HttpGet]
        public ActionResult SaveImageFolder()
        {
            return View();
        }

        public ActionResult DeleteObjet(int id)
        {
            daoObjet.delete(id);
            return RedirectToAction("AfficherObjet");
        }

        [HttpGet]
        public ActionResult AfficherUnObjet(int id)
        {
            DAL.Object ob = daoObjet.read(id);

            return View(ob);
        }

        [HttpGet]
        public ActionResult EditObjet(int id)
        {
            DAL.Object obj = daoObjet.read(id);

            return View(obj);
        }


        [HttpPost]
        public ActionResult EditObjet(DAL.Object obj)
        {
            obj = daoObjet.update(obj);
            if (obj == null) {
                ViewBag.Message = "not possible !";
                return View(obj.IdObjet);

            }

            return RedirectToAction("AfficherObjet", new { id = obj.IdObjet });
        }

        public JsonResult traitementAjax(int valeurId)
        {
            DAL.Object ob = daoObjet.read(valeurId);
            SessionUtils.Panier.Add(ob);
            return Json(SessionUtils.Panier, JsonRequestBehavior.AllowGet); //c'est la façon dont il faut que cela réponde on spécifie le comportement de Json

        }

        public ActionResult afficherPannier()
        {

            return View(SessionUtils.Panier); //revoyer juste une vue du panier
        }
    }

}  





    