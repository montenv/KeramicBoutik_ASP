﻿using magasin.DAL;
using magasin.web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace magasin.web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Profil pro = SessionUtils.UserConnected;
            return View(pro);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {


            //if (SessionUtils.UserConnected != null)
            //{

            //    return RedirectToAction("Index");
            //}
            return View();
        }

        [HttpPost]
        public ActionResult Register(Profil pro)
        {

            DAOProfil daoProfil = new DAOProfil();

            Profil prop = daoProfil.create(pro);
            return RedirectToAction("Index");
        }


    }

}