﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
    public class DAOUser : DAO, IDAO<User>
    {
        public Profil Connection(string login, string password)
        {

            SqlCommand command = connection.CreateCommand();
            string query = "SELECT idUser FROM [User] WHERE login = @login AND password = @password";
            command.Parameters.AddWithValue("@login", login);
            command.Parameters.AddWithValue("@password", password);

            command.CommandText = query;

            int id;
            try
            {
                connection.Open();
                id = (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                connection.Close();
                return null;
            }

            finally
            {
                connection.Close();
            }



            query = "SELECT * FROM profil WHERE fk_User = " + id;
            command.CommandText = query;
            connection.Open();
            try
            {
                id = (int)command.ExecuteScalar();
                DAOProfil daopro = new DAOProfil();
                connection.Close();
                return daopro.read(id);
            }
            catch (Exception ex)
            {
                return null;
            }


        }

        public User create(User objet)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }


        public User read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT idUser, login, password FROM utilisateur WHERE idUser = " + id;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);
            User user = null;
            foreach (DataRow row in dt.Rows)
            {

                user = new User((int)row["idUser"], row["login"].ToString(), row["password"].ToString());
            }

            return user;
        }

        public List<User> readAll()
        {
            throw new NotImplementedException();
        }

        User IDAO<User>.read(int id)
        {
            throw new NotImplementedException();
        }

        List<User> IDAO<User>.readAll()
        {
            throw new NotImplementedException();
        }

        public User update(User objet)
        {
            throw new NotImplementedException();
        }
    }
}

