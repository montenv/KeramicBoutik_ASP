﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
   public class Profil 
    {
        public int IdProfil { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = ":moins de 50 caractères")]
        [Display(Name = "Nom")]
        public string Nom { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = ":moins de 50 caractères")]
        [Display(Name = "Prenom")]
        public string Prenom { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Votre adress mail")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Must be a valid Email Address")]
        public string Email { get; set; }

        public User User { get; set; }

        public Profil()
        {
        }

        public Profil(int idProfil, string nom, string prenom, string email)
        {
            IdProfil = idProfil;
            Nom = nom;
            Prenom = prenom;
            Email = email;

        }
        public Profil( string nom, string prenom, string email,User user)
        {
            
            Nom = nom;
            Prenom = prenom;
            Email = email;
            User = user;
        }


        public Profil(int idProfil, string nom, string prenom, string email, User user) : this(nom, prenom, email,user)
        {
            IdProfil = idProfil;
           
        }



    }
}
