﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
    interface IDAO<T>
    {

        T create(T objet);

        T read(int id);

        List<T> readAll();

        T update(T objet);

        void delete(int id);
    }
}
