﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
    public class User 
    {
        public int IdUser { get; set; }

        [Required]
        public string Login { get; set; }

        [RegularExpression(@"^.{6,}$",
            ErrorMessage = "Minimum 6 characters required")]
        [Required(ErrorMessage = "Password de minimum 6 charactères")]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        public User()
        {

        }

        public User(string login, string password)
        {
            Login = login;
            Password = password;
        }

        public User(int idUser, string login, string password) : this(login, password)
        {
            IdUser = idUser;

        }
    }
}