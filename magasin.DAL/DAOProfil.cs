﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
   public class DAOProfil : DAO, IDAO<Profil>
    {
        public Profil create(Profil objet)
        {

            SqlCommand command = connection.CreateCommand();



            string query = "INSERT INTO [User] (login, password) OUTPUT inserted.idUser VALUES(@login, @password)";
            command.Parameters.AddWithValue("@login", objet.User.Login);
            command.Parameters.AddWithValue("@password", objet.User.Password);
            command.CommandText = query;
            connection.Open();
            int idUser = (int)command.ExecuteScalar();
            connection.Close();
            objet.IdProfil = idUser;


            query = "INSERT INTO Profil (nom, prenom,email, fk_user) OUTPUT inserted.idProfil VALUES (@nom, @prenom,@email, @iduser)";
            command.Parameters.AddWithValue("@nom", objet.Nom);
            command.Parameters.AddWithValue("@prenom", objet.Prenom);
            command.Parameters.AddWithValue("@email", objet.Email);
            command.Parameters.AddWithValue("@iduser", idUser);
            command.CommandText = query;
            connection.Open();
            int idpro = (int)command.ExecuteScalar();
            connection.Close();


            return objet;

        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Profil read(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT idProfil, nom, prenom, email FROM profil WHERE idProfil = " + id;
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;

            DataTable dt = new DataTable();
            da.Fill(dt);
            Profil profil = null;

            foreach (DataRow row in dt.Rows)
            {
                profil = new Profil((int)row["idProfil"], row["nom"].ToString(), row["prenom"].ToString(), row["email"].ToString());
            }
            return profil;
        }

        public List<Profil> readAll()
        {
            throw new NotImplementedException();
        }

        public Profil update(Profil objet)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "UPDATE profil SET nom = @nom, prenom = @prenom, email = @email WHERE idprofil = @id";
            command.CommandText = query;
            command.Parameters.AddWithValue("@nom", objet.Nom);
            command.Parameters.AddWithValue("@prenom", objet.Prenom);
            command.Parameters.AddWithValue("@dDN", objet.Email);
           
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            return objet;
        }
    }
}
