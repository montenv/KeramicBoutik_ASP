﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
    public class Object 
    {
        public int IdObjet { get; set; }
        public string Description { get; set; }
        public float Prix { get; set; }
        public string Photo { get; set; }
        public Categorie Categorie { get; set; }
        public Commande Commande { get; set; }


        public Object()
        {
        }

        public Object(int idObjet, string description, float prix, string photo)
        {
            IdObjet = idObjet;
            Description = description;
            Prix= prix;
            Photo = photo;

        }
        public Object(string description, float prix, string photo,Categorie categorie)
        {

            Description = description;
            Prix = prix;
            Photo = photo;
            Categorie = categorie;
        }

        public Object(string description, float prix, string photo, Categorie categorie, Commande commande)
        {

            Description = description;
            Prix = prix;
            Photo = photo;
            Categorie = categorie;
            Commande = commande;
        }


        public Object (int idObjet, string description, float prix, string photo, Categorie categorie, Commande commande) : this(description,prix,photo,categorie,commande)
        {
            IdObjet = idObjet;

        }

    }
}
