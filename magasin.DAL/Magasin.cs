﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
    class Magasin
    {
        public int IdMagasin { get; set; }
        public string NomMagasin { get; set; }

        public Magasin()
        {

        }

        public Magasin(int idMagasin, string nomMagasin)
        {
            IdMagasin = idMagasin;
            NomMagasin = nomMagasin;
        }
    }
}
