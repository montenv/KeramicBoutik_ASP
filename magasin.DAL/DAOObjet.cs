﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
    public class DAOObjet : DAO, IDAO<Object>
    {
        public Object create(Object objet)
        {         
                SqlCommand command = connection.CreateCommand();

                string query = "INSERT INTO Objet (description,prix,photo,fk_categorie) OUTPUT inserted.idObjet VALUES (@description, @prix,@photo, @fk_categorie)";
                command.Parameters.AddWithValue("@description", objet.Description);
                command.Parameters.AddWithValue("@prix", objet.Prix);
                command.Parameters.AddWithValue("@photo", objet.Photo);
                command.Parameters.AddWithValue("@fk_categorie", objet.Categorie.IdCategorie);
                command.CommandText = query;
                connection.Open();
                int idpro = (int)command.ExecuteScalar();
                connection.Close();
                objet.IdObjet = idpro;
                return objet;


            
        }

        public void delete(int id)
        {
            SqlCommand command = connection.CreateCommand();
            string query = "DELETE FROM Objet WHERE idObjet = " + id;
            command.CommandText = query;
            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                connection.Close();
            } 
        }

        public Object read(int id)
        {
             


            SqlCommand command = connection.CreateCommand();

            string query = "SELECT * FROM Objet WHERE idObjet = " + id;
            command.CommandText = query;
            command.Parameters.AddWithValue("idObjet", id);
            connection.Open();
            SqlDataReader rdr = command.ExecuteReader();
            Object objet = null;

            while (rdr.Read())
            {
                objet = new Object
                {
                    IdObjet = (int)rdr["idObjet"],
                    Description = (string)rdr["description"],
                    Prix = Convert.ToSingle(rdr["prix"]),
                    Photo = (string)rdr["photo"],
                };

            }
            rdr.Close();
            connection.Close();
            return objet;
        }


        public List<Object> readAll()
        {
            List<Object> listObj = new List<Object>();
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT * FROM Objet";
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            DAOObjet daoObj = new DAOObjet();
            foreach (DataRow row in dt.Rows)

            {
                Object obj = new Object();
                obj.Description = row["description"].ToString();
                obj.Prix = Convert.ToSingle(row["prix"]);
                obj.Photo= row["photo"].ToString();
                obj.IdObjet = (int)row["idObjet"];

                listObj.Add(obj);

            }

            return listObj;
        }

        public Object update(Object objet)
        {
            SqlCommand command = connection.CreateCommand();

            string query = "UPDATE Objet SET description = @description, prix = @prix WHERE idObjet = @id";
            command.CommandText = query;
            command.Parameters.AddWithValue("@description", objet.Description);
            command.Parameters.AddWithValue("@prix", objet.Prix);
            command.Parameters.AddWithValue("@id", objet.IdObjet);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            return objet;
        }
    }
}
