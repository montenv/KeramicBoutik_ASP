﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
   public class Commande
    {
        public int IdCategorie { get; set; }
        public string NomCategorie { get; set; }

        public Commande()
        {

        }

        public Commande(int idCategorie, string nomCategorie)
        {
            IdCategorie = idCategorie;
            NomCategorie = nomCategorie;
        }

    }
}
