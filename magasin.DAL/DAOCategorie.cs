﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace magasin.DAL
{
    public class DAOCategorie : DAO, IDAO<Categorie>
    {
        public Categorie create(Categorie objet)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public Categorie read(int id)
        {
            throw new NotImplementedException();
        }

        public List<Categorie> readAll()
        {
            List<Categorie> listCat = new List<Categorie>();
            SqlCommand command = connection.CreateCommand();
            string query = "SELECT idCategorie, nomCategorie FROM Categorie";
            command.CommandText = query;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = command;
            DataTable dt = new DataTable();
            da.Fill(dt);

            DAOCategorie daoCat = new DAOCategorie();
            foreach (DataRow row in dt.Rows)

            {
                Categorie cat= new Categorie();
                cat.NomCategorie = row["nomCategorie"].ToString();
                cat.IdCategorie = (int)row["idCategorie"];

                listCat.Add(cat);

            }

            return listCat;

        }


    

        public Categorie update(Categorie objet)
        {
            throw new NotImplementedException();
        }
    }
}
